package com.znap.mpaymedemo.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.znap.mpaymedemo.app.asynctask.LoginAsyncTask;
import com.znap.mpaymedemo.app.communication.ApiConnector;

public class LoginActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(isUserAlreadySignIn()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        setSubmitLogin();
    }

    /**
     * Check if we already have the md5 or not
     * @return boolean
     */
    private boolean isUserAlreadySignIn() {
        SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);

        return (preferenceManager.contains("usernameMD5") && !preferenceManager.getString("usernameMD5","").isEmpty());
    }

    /**
     * Set the behaviours when the user can submit
     */
    private void setSubmitLogin(){
        // Set the behaviour when the user click in the button "go" in the softkeyboard
        EditText editText = (EditText) findViewById(R.id.editUsername);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    login();
                    handled = true;
                }
                return handled;
            }
        });

        //set method when the user click the submit button
        Button loginButton = (Button) findViewById(R.id.buttonLogin);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    /**
     * Method that check if the username is empty, if has connection and send the request
     */
    private void login(){
        String userName = ((EditText) findViewById(R.id.editUsername)).getText().toString();

        if(userName.isEmpty()){
            Toast.makeText(this, getResources().getString(R.string.username_is_empty), Toast.LENGTH_SHORT).show();
            return;
        }

        if(!new ApiConnector().haveNetworkConnection(this)){
            Toast.makeText(this, getResources().getString(R.string.not_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        findViewById(R.id.container).setVisibility(View.GONE);
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        new LoginAsyncTask(this).execute(userName);
    }

}
