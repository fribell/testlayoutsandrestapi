package com.znap.mpaymedemo.app;

import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.znap.mpaymedemo.app.asynctask.DataAsyncTask;
import com.znap.mpaymedemo.app.communication.ApiConnector;

public class MainActivity extends ActionBarActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Open camera without take care if we want get the picture or not
     * @param view
     */
    public void openCamera(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivity(intent);
    }

    /**
     * Open webBrowser with specific url
     * @param view
     */
    public void openWebBrowser(View view){
        Uri url = Uri.parse("http://www.mpayme.com/");
        Intent intent = new Intent(Intent.ACTION_VIEW, url);
        startActivity(intent);
    }

    /**
     * Show toast message
     * @param view
     */
    public void showToastMessage(View view){
        Toast.makeText(this, getResources().getString(R.string.app_name), Toast.LENGTH_LONG).show();
    }

    /**
     * Get headers information and open the data activity if everything is fine
     * @param view
     */
    public void openDataActivity(View view){
        if(!new ApiConnector().haveNetworkConnection(this)){
            Toast.makeText(this, getResources().getString(R.string.not_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        new DataAsyncTask(this).execute(DataAsyncTask.MAIN_ACTIVITY);
    }
}
