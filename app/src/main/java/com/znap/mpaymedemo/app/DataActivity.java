package com.znap.mpaymedemo.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.znap.mpaymedemo.app.adapters.MyHeaderArrayAdapter;
import com.znap.mpaymedemo.app.asynctask.DataAsyncTask;
import com.znap.mpaymedemo.app.communication.ApiConnector;
import com.znap.mpaymedemo.app.objects.MyHeader;

import java.util.ArrayList;

public class DataActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(getIntent().hasExtra("myHeaders")){
            ArrayList<MyHeader> myHeaders = (ArrayList<MyHeader>) getIntent().getSerializableExtra("myHeaders");
            fillListView(this, myHeaders);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // I did this for old versions of API, the new ones the attribute "parentActivityName" in the manifest is enough
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    /**
     * Fill listView with a custom adapter, is static because I'm calling this method from the Async Task
     * @param myHeaders
     */
    public static void fillListView(Context context, ArrayList<MyHeader> myHeaders) {
        ListView listView = (ListView) ((Activity) context).findViewById(R.id.listView);

        MyHeaderArrayAdapter adapter = new MyHeaderArrayAdapter(
                context,
                android.R.layout.simple_list_item_2,
                myHeaders );

        listView.setAdapter(adapter);
    }

    /**
     * Refresh the listView calling again the httpPost
     * @param view
     */
    public void refreshList(View view){
        if(!new ApiConnector().haveNetworkConnection(this)){
            Toast.makeText(this, getResources().getString(R.string.not_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        new DataAsyncTask(this).execute(DataAsyncTask.DATA_ACTIVITY);
    }

}
