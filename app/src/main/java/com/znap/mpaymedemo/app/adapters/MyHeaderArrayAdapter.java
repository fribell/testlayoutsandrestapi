package com.znap.mpaymedemo.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.znap.mpaymedemo.app.R;
import com.znap.mpaymedemo.app.objects.MyHeader;

import java.util.ArrayList;

/**
 * Created by ferran on 06/03/14.
 */
public class MyHeaderArrayAdapter extends ArrayAdapter<MyHeader> {

    private int layoutResourceId;
    private ArrayList<MyHeader> myHeaders;

    public static class ViewHolder {
        public TextView name;
        public TextView value;
    }

    public MyHeaderArrayAdapter(Context context, int layoutResourceId, ArrayList<MyHeader> myHeaders) {
        super(context, layoutResourceId, myHeaders);
        this.layoutResourceId = layoutResourceId;
        this.myHeaders = myHeaders;
    }

    public int getCount() {
        return myHeaders.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(layoutResourceId, null);
            holder = new ViewHolder();

            holder.name = (TextView) vi.findViewById(android.R.id.text1);
            holder.value = (TextView) vi.findViewById(android.R.id.text2);

            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        holder.name.setText(myHeaders.get(position).getName());
        holder.value.setText(myHeaders.get(position).getValue());
        if (position % 2 != 0) {
            vi.setBackgroundColor(getContext().getResources().getColor(R.color.grey));
        }

        return vi;
    }
}
