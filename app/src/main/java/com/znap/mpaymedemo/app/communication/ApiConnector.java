package com.znap.mpaymedemo.app.communication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.znap.mpaymedemo.app.objects.MyHeader;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

/**
 * Created by ferran on 06/03/14.
 */
public class ApiConnector {
    private static final String TAG = "API CONNECTION";
    private static final int TIMEOUT_CONNECTION = 5000;
    private static final int TIMEOUT_SOCKET = 5000;
    private static final int RESPONSE_200 = 200;

    /**
     * Send request to get the md5 of the username
     * @param userName
     * @return String
     */
    public String submitLogin(String userName){
        HttpClient httpClient = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), TIMEOUT_CONNECTION);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), TIMEOUT_SOCKET);

        HttpGet requestGet = new HttpGet("http://md5.jsontest.com/?text="+userName);
        InputStream is = null;
        try {
            HttpResponse response = httpClient.execute(requestGet);

            if (response.getStatusLine().getStatusCode() != RESPONSE_200 ) {
                return null;
            }

            HttpEntity entity = response.getEntity();
            is = entity.getContent();
            return readIt(is);

        } catch (ConnectTimeoutException e) {
            Log.e(TAG, "Timeout error: " + e.getMessage());
        } catch (SocketTimeoutException e) {
            Log.e(TAG, "Data retrieval or connection timed out");
        } catch (IOException e) {
            Log.e(TAG, "Server error: " + e.getMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.e(TAG, "Closing inputStream " + e.getMessage());
                }
            }
        }

        return null;
    }

    /**
     * Send request to some url and get the list of headers
     * @return
     */
    public ArrayList<MyHeader> getData() {
        HttpClient httpClient = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), TIMEOUT_CONNECTION);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), TIMEOUT_SOCKET);

        HttpPost requestPost = new HttpPost("http://httpbin.org/post");
        try {
            HttpResponse response = httpClient.execute(requestPost);

            if (response.getStatusLine().getStatusCode() != RESPONSE_200 ) {
                return null;
            }

            return getAllHeaders(response);

        } catch (ConnectTimeoutException e) {
            Log.e(TAG, "Timeout error: " + e.getMessage());
        } catch (SocketTimeoutException e) {
            Log.e(TAG, "Data retrieval or connection timed out");
        } catch (IOException e) {
            Log.e(TAG, "Server error: " + e.getMessage());
        }

        return null;
    }

    /**
     * Method to transform all the headers to Map
     * @param response
     * @return Map<String, String>
     */
    private ArrayList<MyHeader> getAllHeaders(HttpResponse response) {
        Header[] origHeaders = response.getAllHeaders();
        ArrayList<MyHeader> myHeaders = new ArrayList<MyHeader>();
        for (Header h : origHeaders) {
            myHeaders.add(new MyHeader(h.getName(), h.getValue()));
        }
        return myHeaders;
    }

    /**
     * Reads an InputStream and converts it to a String.
     * @param stream
     * @return String
     * @throws IOException
     */
    private String readIt(InputStream stream) throws IOException {
        StringBuilder content = new StringBuilder(stream.available());
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            content.append(line);
        }
        bufferedReader.close();

        return content.toString();
    }

    /**
     * Check if we have connection in the device
     * @param context
     * @return boolean
     */
    public boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
