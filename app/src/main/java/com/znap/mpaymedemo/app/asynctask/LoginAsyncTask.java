package com.znap.mpaymedemo.app.asynctask;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.znap.mpaymedemo.app.MainActivity;
import com.znap.mpaymedemo.app.R;
import com.znap.mpaymedemo.app.communication.ApiConnector;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ferran on 06/03/14.
 */
public class LoginAsyncTask extends AsyncTask<String, Void, String> {
    private Context context;

    public LoginAsyncTask(Context context){
        this.context = context;
    }

    protected String doInBackground(String... username) {
        String response = new ApiConnector().submitLogin(username[0]);

        if(response == null) return null;
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(!jsonObject.has("md5")) return null;

            return jsonObject.getString("md5");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    protected void onPostExecute(String md5) {
        if(md5 != null){
            saveMD5(md5);

            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);
            ((Activity) context).finish();
        }else{
            Toast.makeText(context, context.getResources().getString(R.string.error_md5), Toast.LENGTH_LONG).show();
            ((Activity) context).findViewById(R.id.container).setVisibility(View.VISIBLE);
            ((Activity) context).findViewById(R.id.progressBar).setVisibility(View.GONE);
        }
    }

    private void saveMD5(String md5) {
        SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferenceManager.edit();
        editor.putString("usernameMD5", md5);
        editor.commit();
    }
}
