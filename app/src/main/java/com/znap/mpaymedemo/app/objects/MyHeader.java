package com.znap.mpaymedemo.app.objects;

import java.io.Serializable;

/**
 * Created by ferran on 06/03/14.
 */
public class MyHeader implements Serializable{
    private String name;
    private String value;

    public MyHeader(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
