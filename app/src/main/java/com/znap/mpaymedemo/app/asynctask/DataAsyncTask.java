package com.znap.mpaymedemo.app.asynctask;

/**
 * Created by ferran on 06/03/14.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.znap.mpaymedemo.app.DataActivity;
import com.znap.mpaymedemo.app.R;
import com.znap.mpaymedemo.app.communication.ApiConnector;
import com.znap.mpaymedemo.app.objects.MyHeader;

import java.util.ArrayList;

/**
 * Created by ferran on 06/03/14.
 */
public class DataAsyncTask extends AsyncTask<Integer, Void, ArrayList<MyHeader>> {
    public static final int MAIN_ACTIVITY = 1;
    public static final int DATA_ACTIVITY = 2;

    private Context context;
    private int whichActivity;
    private ProgressDialog progressDialog;

    public DataAsyncTask(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getResources().getString(R.string.loading));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
            }
        });
        progressDialog.show();
    }

    protected ArrayList<MyHeader> doInBackground(Integer... args) {
        whichActivity = args[0];
        return new ApiConnector().getData();
    }

    protected void onPostExecute(ArrayList<MyHeader> myHeaders) {
        progressDialog.dismiss();
        if(isCancelled()) return;

        if(myHeaders != null){
            switch (whichActivity){
                case MAIN_ACTIVITY:
                    Intent intent = new Intent(context, DataActivity.class);
                    intent.putExtra("myHeaders", myHeaders);
                    context.startActivity(intent);
                    break;
                case DATA_ACTIVITY:
                    DataActivity.fillListView(context, myHeaders);
                    break;
            }
        }else{
            Toast.makeText(context, context.getResources().getString(R.string.error_headers), Toast.LENGTH_LONG).show();
        }
    }

}
